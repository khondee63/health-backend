# Stage 1: Build stage using Maven to build the application JAR
FROM maven:3.8.4-openjdk-17 AS build

# Set the working directory inside the container
WORKDIR /app

# Copy the Maven build files and source code into the container
COPY . .

# Run Maven to build the application JAR file
RUN mvn clean package -DskipTests

# Stage 2: Create a new stage with a lightweight OpenJDK image
FROM openjdk:17-jdk-slim

# Set the working directory inside the container
WORKDIR /app

# Expose port 8080 to the outside world
EXPOSE 8080

# Copy the JAR file built in the previous stage into the container
COPY --from=build /app/target/*.jar app.jar

# Command to run the application when the container starts
ENTRYPOINT ["java", "-jar", "app.jar"]
