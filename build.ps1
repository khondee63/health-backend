# กำหนดชื่อของแอปพลิเคชัน
$app = "heathcareweb"

# กำหนดเวอร์ชันหรือแท็กของแอปพลิเคชัน
$tag = "1.0"

# สั่ง Maven ให้ทำการ clean และ package โปรเจกต์ โดยข้ามการทดสอบ (skipTests)
mvn clean package -DskipTests

# สร้าง Docker image โดยใช้ชื่อและแท็กที่กำหนด
docker build -t "${app}:${tag}" .


