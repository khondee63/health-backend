package health.backend.util;
import health.backend.model.UserAccountModel;
import io.jsonwebtoken.*;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.security.sasl.AuthenticationException;
import java.util.Date;
import java.util.UUID;
@Component
@Slf4j

public class JwtUtil {
    @Value("testsecretkey")
    private String secretKey;
    @Value("86400000")
    private Long timeExpire;
    private final String TOKEN_HEADER = "Authorization";
    private final String TOKEN_PREFIX = "Bearer ";
    private JwtParser jwtParser;

    @PostConstruct
    public void postInit() {
        jwtParser = Jwts.parser().setSigningKey(secretKey);
    }

    public String generateToken(UserAccountModel request) {
        var milliseconds = timeExpire * 1000;
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + milliseconds);
        Claims claims = Jwts.claims().setSubject(UUID.randomUUID().toString());
        claims.put("_id",request.get_id());
        claims.put("username", request.getUsername());
        claims.put("password", request.getPassword());
        claims.put("status",request.getStatus());
        claims.setIssuedAt(now);
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }
    private Claims parseJwtClaims(String token) {
        return jwtParser.parseClaimsJws(token).getBody();
    }

    public Claims resolveClaims(HttpServletRequest req) {
        try {
            String token = resolveToken(req);
            if (token != null) {
                return parseJwtClaims(token);
            }
            return null;
        } catch (ExpiredJwtException ex) {
            req.setAttribute("expired", ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            req.setAttribute("invalid", ex.getMessage());
            throw ex;
        }
    }

    public String resolveToken(HttpServletRequest request) {

        String bearerToken = request.getHeader(TOKEN_HEADER);
        if (bearerToken != null && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(TOKEN_PREFIX.length());
        }
        return null;
    }
    public boolean validateClaims(Claims claims) throws AuthenticationException {
        try {
            return claims.getExpiration().after(new Date());
        } catch (Exception e) {
            throw e;
        }
    }


}
