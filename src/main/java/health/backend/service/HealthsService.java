package health.backend.service;
import health.backend.model.HealthsModel;
import health.backend.repository.HealthsRepository;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.List;
@Service
public class HealthsService {
    private HealthsRepository healthsRepository;

    public HealthsService(HealthsRepository healthsRepository) {
        this.healthsRepository = healthsRepository;
    }
    public String insertHealthData(HealthsModel request){
        HealthsModel healthsModel = new HealthsModel();
        healthsModel.setUserId(request.getUserId());
        healthsModel.setPulse(request.getPulse());
        healthsModel.setDiasTolic(request.getDiasTolic());
        healthsModel.setOxyGen(request.getOxyGen());
        healthsModel.setSysTolic(request.getSysTolic());
        healthsModel.setCreatedAt(LocalDateTime.now());
        healthsModel.setUpdatedAt(LocalDateTime.now());
        healthsRepository.save(healthsModel);
        List<HealthsModel> id= healthsRepository.findAll();
        return id.get(0).get_id();
    }

    public List<HealthsModel> getHealthData(String userId) {
        return healthsRepository.findAllByUserId(userId);
    }
}
