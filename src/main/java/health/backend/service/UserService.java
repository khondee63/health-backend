package health.backend.service;
import health.backend.model.HealthsModel;
import health.backend.model.PhotoHealthModel;
import health.backend.model.UserAccountModel;
import health.backend.model.request.LoginRequestModel;
import health.backend.model.request.UserUpdateRequestModel;
import health.backend.model.response.*;
import health.backend.repository.HealthsRepository;
import health.backend.repository.PhotoHealthsRepository;
import health.backend.repository.UserRepository;
import health.backend.util.JwtUtil;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
@Service
@Slf4j

public class UserService {
    private UserRepository userRepository;
    private PhotoHealthsRepository photoHealthsRepository;
    private HealthsRepository healthsRepository;
    private JwtUtil jwtUtil;


    public UserService(UserRepository userRepository, PhotoHealthsRepository photoHealthsRepository, HealthsRepository healthsRepository, JwtUtil jwtUtil) {
        this.userRepository = userRepository;
        this.photoHealthsRepository = photoHealthsRepository;
        this.healthsRepository = healthsRepository;
        this.jwtUtil = jwtUtil;
    }
    public void registerUser(UserAccountModel request){
        if (request.getUsername() == null || request.getUsername().isEmpty()) {
            throw new IllegalArgumentException("Username cannot be empty");
        }
        if (request.getPassword() == null || request.getPassword().isEmpty()) {
            throw new IllegalArgumentException("Password cannot be empty");
        }
        if (!userRepository.findByUsername(request.getUsername()).isEmpty()) {
            throw new IllegalArgumentException("Username is already taken");
        }
        if (request.getStatus() == null || request.getStatus().isEmpty()) {
            request.setStatus("user");
        }
        userRepository.save(request);
    }

    public LoginResponseModel processLogin(LoginRequestModel request) throws Exception {
        try {
            List<UserAccountModel> userAccountModel = userRepository.findByUsername(request.getUsername());
            if(CollectionUtils.isEmpty(userAccountModel)) {
                log.info("ไม่พบ user");
                throw new Exception("ไม่พบผู้ใช้");
            }
            List<UserAccountModel> user = userRepository.findByUsernameAndPassword(userAccountModel.get(0).getUsername(), request.getPassword());
            if(CollectionUtils.isEmpty(user)) {
                log.info("user หรือ password ผิด");
                throw new Exception("ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง");
            }
            String accessToken = jwtUtil.generateToken(userAccountModel.get(0));
            String status = userAccountModel.get(0).getStatus();
            String _id = userAccountModel.get(0).get_id();
            return LoginResponseModel.builder()
                    .accessToken(accessToken)
                    .status(status)
                    ._id(_id)
                    .build();
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
    public List<AllDataResponseModel> getAllData() {
        List<HealthsModel> healths = healthsRepository.findAll();
        List<PhotoHealthModel> photoHealths = photoHealthsRepository.findAll();
        List<UserAccountModel> users = userRepository.findAll();
        return users.stream().map(user -> {
            AllDataResponseModel responseModel = new AllDataResponseModel();
            responseModel.setUserId(user.get_id());
            responseModel.setUsername(user.getUsername());
            HealthsModel health = healths.stream()
                    .filter(h -> h.getUserId().equals(user.get_id()))
                    .findFirst().orElse(null);
            if (health != null) {
                responseModel.setOxyGen(health.getOxyGen());
                responseModel.setSysTolic(health.getSysTolic());
                responseModel.setDiasTolic(health.getDiasTolic());
                responseModel.setPulse(health.getPulse());
            }
            PhotoHealthModel photoHealth = photoHealths.stream()
                    .filter(ph -> ph.getUserId().equals(user.get_id()))
                    .findFirst().orElse(null);
            if (photoHealth != null) {
                responseModel.setOxiMeterImage(photoHealth.getOxiMeterImage());
                responseModel.setBloodPressureMeterImage(photoHealth.getBloodPressureMeterImage());
            }
            responseModel.setCreatedAt(user.getCreatedAt());
            responseModel.setUpdatedAt(user.getUpdatedAt());

            return responseModel;
        }).collect(Collectors.toList());
    }

    public void updateUser(HttpServletRequest request, @RequestBody UserUpdateRequestModel request2) throws Exception {
        // แกะข้อมูลจาก JWT
        Claims claims = jwtUtil.resolveClaims(request);
        String userId = (String) claims.get("_id");
        String username = (String) claims.get("username");
        String password = (String) claims.get("password");

        // ตรวจสอบว่าข้อมูลที่ส่งมาเป็นของผู้ใช้ที่ล็อกอินอยู่หรือไม่
        if (username != null && password != null && userId != null) {
            // หากเป็นของผู้ใช้ที่ล็อกอินอยู่ ก็ค่อยทำการอัปเดตข้อมูล
            UserAccountModel userAccountModel = userRepository.findBy_id(userId);

            if (userAccountModel != null) {
                // อัปเดตรหัสผ่านเท่านั้นเนื่องจากไม่ควรอัปเดต username ในกรณีนี้
                userAccountModel.setPassword(request2.getNewpassword());
                userRepository.save(userAccountModel);
            } else {
                throw new Exception("ไม่พบข้อมูลผู้ใช้");
            }
        } else {
            throw new Exception("ข้อมูลผู้ใช้ไม่ถูกต้องหรือไม่มีสิทธิ์ในการอัปเดต");
        }
    }

    public MyDataResponseModel getMyData(HttpServletRequest request) {
        Claims claims = jwtUtil.resolveClaims(request);
        String userId = (String) claims.get("_id");
        HealthsModel health = healthsRepository.findFirstByUserIdOrderByCreatedAtDesc(userId);
      PhotoHealthModel photoHealthModel = photoHealthsRepository.findFirstByUserIdOrderByCreatedAtDesc(userId);
      MyDataResponseModel myDataResponseModel = new MyDataResponseModel();
      myDataResponseModel.setOxyGen(health.getOxyGen());
      myDataResponseModel.setSysTolic(health.getSysTolic());
      myDataResponseModel.setDiasTolic(health.getDiasTolic());
      myDataResponseModel.setPulse(health.getPulse());
      myDataResponseModel.setOxiMeterImage(photoHealthModel.getOxiMeterImage());
      myDataResponseModel.setBloodPressureMeterImage(photoHealthModel.getBloodPressureMeterImage());
      return myDataResponseModel;
    }

    public String getNameData(HttpServletRequest request) {
      Claims claims = jwtUtil.resolveClaims(request);
      String username = (String) claims.get("username");
      return username;
    }
}


