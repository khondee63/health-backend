package health.backend.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AiService {
    private final RestTemplate restTemplate;

    public AiService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void callPythonAPI(String apiUrl) {
        // ทำการเรียก Flask API ด้วย POST request โดยไม่ส่งพารามิเตอร์
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(apiUrl, null, String.class);

        // ตรวจสอบ response ที่ได้จาก Flask API
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            // ดำเนินการตามที่ต้องการหลังจากได้รับ response สำเร็จ
            String responseBody = responseEntity.getBody();
            System.out.println("Response from Flask API: " + responseBody);
        } else {
            // ดำเนินการตามที่ต้องการหลังจากได้รับ response ไม่สำเร็จ
            System.out.println("Failed to call Flask API. Status code: " + responseEntity.getStatusCodeValue());
        }
    }
}
