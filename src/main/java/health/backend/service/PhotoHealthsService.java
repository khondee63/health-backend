package health.backend.service;
import health.backend.model.HealthsModel;
import health.backend.model.PhotoHealthModel;
import health.backend.model.response.MyDataResponseModel;
import health.backend.repository.PhotoHealthsRepository;
import health.backend.util.JwtUtil;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
@Service
@Slf4j
public class PhotoHealthsService {
    private PhotoHealthsRepository photoHealthsRepository;
    private JwtUtil jwtUtil;

    public PhotoHealthsService(PhotoHealthsRepository photoHealthsRepository, JwtUtil jwtUtil) {
        this.photoHealthsRepository = photoHealthsRepository;
        this.jwtUtil = jwtUtil;
    }

    public String savePhoto(String userId, MultipartFile oxiMeterImage, MultipartFile bloodPressureMeterImage) throws IOException {
        PhotoHealthModel photoHealthModel = new PhotoHealthModel();
        photoHealthModel.setUserId(userId);
        photoHealthModel.setOxiMeterImage(oxiMeterImage.getBytes());
        photoHealthModel.setBloodPressureMeterImage(bloodPressureMeterImage.getBytes());
        photoHealthModel.setCreatedAt(LocalDateTime.now());
        photoHealthModel.setUpdatedAt(LocalDateTime.now());
        photoHealthsRepository.save(photoHealthModel);
        List<PhotoHealthModel> id= photoHealthsRepository.findAll();
        return id.get(0).get_id();
    }

    public List<PhotoHealthModel> getUserImages(String userId) {
        List<PhotoHealthModel> photoHealthModel = photoHealthsRepository.findAllByUserId(userId);
        log.info("data",photoHealthModel);
        log.info("userid",userId);

        return photoHealthModel;
    }
    public String savePhotoMe(HttpServletRequest request, MultipartFile oxiMeterImage, MultipartFile bloodPressureMeterImage) throws IOException {
        PhotoHealthModel photoHealthModel = new PhotoHealthModel();
        Claims claims = jwtUtil.resolveClaims(request);
        String userId = (String) claims.get("_id");
        photoHealthModel.setUserId(userId);
        photoHealthModel.setOxiMeterImage(oxiMeterImage.getBytes());
        photoHealthModel.setBloodPressureMeterImage(bloodPressureMeterImage.getBytes());
        photoHealthModel.setCreatedAt(LocalDateTime.now());
        photoHealthModel.setUpdatedAt(LocalDateTime.now());
        photoHealthsRepository.save(photoHealthModel);
        List<PhotoHealthModel> id= photoHealthsRepository.findAll();
        return id.get(0).get_id();
    }

    public List<PhotoHealthModel> getAllImages(HttpServletRequest request) {
        Claims claims = jwtUtil.resolveClaims(request);
        String userId = (String) claims.get("_id");
        List<PhotoHealthModel> photoHealthModel = photoHealthsRepository.findAllByUserId(userId);
        return photoHealthModel;
    }
    //เปลี่ยนลำดับ จากรูป ใหม่ อยู่บนสุด
}

