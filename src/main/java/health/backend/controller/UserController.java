package health.backend.controller;
import health.backend.model.UserAccountModel;
import health.backend.model.request.LoginRequestModel;
import health.backend.model.request.UserUpdateRequestModel;
import health.backend.model.response.AllDataResponseModel;
import health.backend.model.response.LoginResponseModel;
import health.backend.model.response.MyDataResponseModel;
import health.backend.model.response.NameDataResponseModel;
import health.backend.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    @PostMapping("/user-register")
    public void registerUser(@RequestBody UserAccountModel request)
            throws Exception{
        userService.registerUser(request);
    }
    @PostMapping(value = "/user-login")
    public LoginResponseModel loginUser(@RequestBody LoginRequestModel request)
            throws Exception {
        return userService.processLogin(request);
    }

    @GetMapping(value = "/all-data-user")
    public List<AllDataResponseModel> getAllData()throws Exception{
        return userService.getAllData();

    }
    @PostMapping(value = "/user-update")
    public void UpdateUser(HttpServletRequest request, @RequestBody UserUpdateRequestModel request2)
    throws Exception
    {
        userService.updateUser(request, request2);
    }

    @GetMapping(value = "/get-me-dataHealth")
    public MyDataResponseModel getMyData(HttpServletRequest request)throws Exception{
        return  userService.getMyData(request);
    }

    @GetMapping(value = "/get-username-account-me")
    public String getNameData(HttpServletRequest request)throws Exception {
        return userService.getNameData(request);
    }
}

