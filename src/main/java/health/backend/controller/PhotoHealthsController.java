package health.backend.controller;
import health.backend.model.PhotoHealthModel;
import health.backend.service.PhotoHealthsService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PhotoHealthsController {
    private PhotoHealthsService photoHealthsService;

    public PhotoHealthsController(PhotoHealthsService photoHealthsService) {
        this.photoHealthsService = photoHealthsService;
    }
    @PostMapping("/upload-image/{userId}")
    public String uploadPhoto(@PathVariable String userId,
                                                @RequestParam("oxiMeterImage") MultipartFile oxiMeterImage,
                                                @RequestParam("bloodPressureMeterImage") MultipartFile bloodPressureMeterImage) throws IOException {
        return photoHealthsService.savePhoto(userId, oxiMeterImage, bloodPressureMeterImage);
    }

    @GetMapping("/get-images/{userId}")
    public List<PhotoHealthModel> getUserImages(@PathVariable String userId) {
        return photoHealthsService.getUserImages(userId);
    }
    @PostMapping("/upload-images-me")
    public  String uploadPhotoAdmin(HttpServletRequest request,
                                    @RequestParam("oxiMeterImage") MultipartFile oxiMeterImage,
                                    @RequestParam("bloodPressureMeterImage") MultipartFile bloodPressureMeterImage) throws IOException {
        return photoHealthsService.savePhotoMe(request, oxiMeterImage, bloodPressureMeterImage);
    }

    @GetMapping("/get-all-images-me")
    public List<PhotoHealthModel> getAllImages(HttpServletRequest request) {
        return photoHealthsService.getAllImages(request);
    }
}

