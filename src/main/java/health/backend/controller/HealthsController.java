package health.backend.controller;
import health.backend.model.HealthsModel;
import health.backend.service.HealthsService;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@RestController
@RequestMapping("/api")
public class HealthsController {
    private HealthsService healthsService;

    public HealthsController(HealthsService healthsService) {
        this.healthsService = healthsService;
    }

    @PostMapping("/insert-data-health")
    public String insertHealthData(@RequestBody HealthsModel request)throws Exception{
        return healthsService.insertHealthData(request);
    }
    @GetMapping("/get-data-health/{userId}")
    public List<HealthsModel> getHealthData(@PathVariable String userId) throws Exception {
        return healthsService.getHealthData(userId);
    }
}
