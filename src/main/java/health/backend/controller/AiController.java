package health.backend.controller;

import health.backend.service.AiService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AiController {
    private final AiService aiService;
    public AiController(AiService aiService) {
        this.aiService = aiService;
    }

    @PostMapping("/call-ai")
    public void callPythonAPI(@RequestParam String apiUrl) {
        aiService.callPythonAPI(apiUrl);
    }
}
