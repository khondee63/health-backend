package health.backend.repository;
import health.backend.model.PhotoHealthModel;
import health.backend.model.UserAccountModel;
import health.backend.model.response.NameDataResponseModel;
import health.backend.model.response.UserAccountModel2;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<UserAccountModel, String> {
    List<UserAccountModel> findByUsername(String username);
    List<UserAccountModel> findByUsernameAndPassword(String username, String password);
    UserAccountModel findBy_id(String _id);

    @Query(value = "{ 'status' : 'user' }", fields = "{ 'username' : 1}")
    UserAccountModel2 findUsernameWithUserStatus();


    Optional<UserAccountModel> findUsernameByUsername(String username);
}