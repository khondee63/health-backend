package health.backend.repository;
import health.backend.model.PhotoHealthModel;
import health.backend.model.UserAccountModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PhotoHealthsRepository extends MongoRepository<PhotoHealthModel, String> {
    List<PhotoHealthModel> findAllByUserId(String userId);
    PhotoHealthModel findFirstByUserIdOrderByCreatedAtDesc(String userId);
}
