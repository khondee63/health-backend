package health.backend.repository;
import health.backend.model.HealthsModel;
import health.backend.model.PhotoHealthModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HealthsRepository extends MongoRepository<HealthsModel, String> {
    List<HealthsModel> findAllByUserId(String userId);
    HealthsModel findFirstByUserIdOrderByCreatedAtDesc(String userId);
}
