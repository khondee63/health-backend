package health.backend.model.response;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;

@Data
public class MyDataResponseModel {
    private Integer oxyGen;
    private Integer sysTolic;
    private Integer diasTolic;
    private Integer pulse;
    private byte[] oxiMeterImage;
    private byte[] bloodPressureMeterImage;
}
