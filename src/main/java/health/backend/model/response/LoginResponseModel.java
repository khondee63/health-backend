package health.backend.model.response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
@Data
@AllArgsConstructor
@Builder
public class LoginResponseModel {
    private String _id;
    private String status;
    private String accessToken;
}
