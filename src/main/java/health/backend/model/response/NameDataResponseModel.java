package health.backend.model.response;

import lombok.Data;

@Data
public class NameDataResponseModel {
    private String username;
}
