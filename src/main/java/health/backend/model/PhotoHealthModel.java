package health.backend.model;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;
@Data
@Document(collection = "Photohealths_Schema")
public class PhotoHealthModel {
    @Id
    private String _id;
    private String userId ;
    private byte[] oxiMeterImage;
    private byte[] bloodPressureMeterImage;
    @CreatedDate
    private LocalDateTime createdAt;
    @LastModifiedDate
    private LocalDateTime updatedAt;
}

