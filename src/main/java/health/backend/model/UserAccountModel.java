package health.backend.model;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document(collection = "Users_Schema")

public class UserAccountModel {
    @Id
    private String _id;
    @NotNull
    @Indexed(unique = true)
    private String username;
    @NotNull
    private String password;
    private String status = "user";
    @CreatedDate
    private LocalDateTime createdAt;
    @LastModifiedDate
    private LocalDateTime updatedAt;

}
