package health.backend.model;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;
@Data
@Document(collection = "Healths_Schema")
public class HealthsModel {
    @Id
    private String _id;
    private String userId;
    private Integer oxyGen;
    private Integer sysTolic;
    private Integer diasTolic;
    private Integer pulse;
    @CreatedDate
    private LocalDateTime createdAt;
    @LastModifiedDate
    private LocalDateTime updatedAt;
}
