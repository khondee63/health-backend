package health.backend.model.request;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
public class LoginRequestModel {
    @NotNull
    private String username;
    @NotNull
    private String password;
}
