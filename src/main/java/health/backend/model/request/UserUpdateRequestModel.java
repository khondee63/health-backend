package health.backend.model.request;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
public class UserUpdateRequestModel {
    @NotNull
    private  String username;
    @NotNull
    private String password;
    @NotNull
    private String newpassword;
}
