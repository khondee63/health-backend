package health.backend.model.request;
import lombok.Data;

@Data
public class PhotoHealthRequestModel {
    private String _id;
    private byte[] oxiMeterImage;
    private byte[] bloodPressureMeterImage;
}
